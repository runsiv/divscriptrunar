
%Clear potentially remaining variable from previous runs (other matrix
%sizes could cause confusion and error
clear addr;

%Set the time to wait after an error before performing the next data
%query. This is to ensure that an error in the swarm, causing a device
%reset, will not effect the next query. For swarm, the time from crash to
%full functionality is regains is maximum 13 seconds (as of fw v0.9.0).
errorwaittime = 0;
%Error message to desplay
errorwaitmsg = ['Error detected. Waiting ' num2str(errorwaittime) ' seconds for system to stabilize.'];

%Have user input the desired communication port (Windows COMPORT)
comport = input('Input Serial port (e.g. "COM2"):\n','s');

%Have user input the desired number of segments to test
swarmcnt = str2num(input('Please enter number of swarms to test:\n','s'));

%Ask user to verify the configuration
answ = 'k';

for cn =1:swarmcnt
    swarm_sn{cn} = input(['Enter serinalnumber of the swarm #' num2str(cn) ':\n'], 's');
    
    %Quarry the user to input the rs 232 address for the first swarm
    addr(cn) =  str2num(input([' Swarm #', num2str(cn) ':Address of swarm 1:\n'],'s'));
end
%Print out the cassette/swarm/address configuration to the user
disp('');
disp('');
disp('');
disp('Configuration is:');

for cn=1:swarmcnt
    disp(['    swarm ' swarmcnt{cn} ':']);
    disp(['         Swarm S1 #1 addr.: ' num2str(addr(cn,1))]);
end

%Ask user to verify the configuration
answ = 'k';
while(lower(answ) ~= 'y' && lower(answ) ~= 'n')
    answ = input('Verify that this configuration is correct (y/n):\n','s');
end
%If configuration is selected as invalid, abort the script
if strcmp(answ,'n')
    disp('Test script aborted by user...');
    return
end

disp('Configuration confirmed by user...');
disp('');
%Have user input the desired test duration
duration = strsplit(input('Enter the desired test duration (HH:MM:SS):\n','s'),':');
%Convert duration string to hours, minutes and seconds
durH = str2num(cell2mat(duration(1)));
durM = str2num(cell2mat(duration(2)));
durS = str2num(cell2mat(duration(3)));

%Have user input the desired iteration interval. This is the interval between
%each full iteration of all devices. E.g. If the test starts at 14:05:00
%and the interval is set to 120 seconds. The next iteration will begin at
%14:07:00. If the interval is shorter than one full system test iteration,
%the test will run continously
delay = str2num(input('Please enter interval between each full iteration: [s]\n','s'));


%Set the maximum allowed number of retries (attempts to query the
%device if the first attempt fails). Minimum value is 1
%max_retries = str2num(input('Enter the maximum allowed retries per data query. Minimum value is 1.'));
%if(max_retries < 1)
    max_retries = 0;
%end;
disp(['Max retries set to ' num2str(max_retries) '.']);

%Create the 'default' file name for output file.
fname = 'cgcm_segment_log.txt';
%Have user select the desired output file name and location
[FileName,PathName] = uiputfile('.txt','Name of output file(s)',fname);


%Set the baudrate for the serial communication
baud = 115200;
%Set the timeout for the serialport. This should be quite high if e.g.
%'averages' is configured higly on swarm, or other 'measurement time
%affeting' settings are configured.
timeout = 3.5;

if(exist('ser'))
    %If 's' exists, check if it is a serial port object.
    if(strcmp(class(ser),'serial'))
        %If it is a serial port object; Try to close is
        try
            fclose(ser);
        catch
            %If closing 'S' fails, clear all 'instruments' from matlab
            %memory.
            fclose(instrfindall);
            delete(instrfindall);
            %Recreate 'S' with the correct baud rate
            ser = serial(comport,'baud',baud);
        end
    else
        %If 'S' is not a serial port object, discard the variable
        clear ser;
        %Create 'S' as serial port object with the correct baud rate
        ser = serial(comport,'baud',baud);
    end

end
try
    fopen(ser);
catch
    %If serial port 'S' could not be opened...
    disp('COM-port not available. Forcing.');
    %Flush all knowns instruments (ports) from matlab memory
    %fclose(instrfindall);
    delete(instrfindall);
    %Recreate port
    ser = serial(comport,'baud',baud);
    
end

%---
%Collecting serial numbers digitally
%---
%Close serial port
fclose(ser);
%Configure COM port for messages of 50-bytes
set(ser,'inputbuffer',28);
%Set timeout to 2 seconds
set(ser,'timeout',2);
%Clear any remaining serial numbers from potential previous test runs
clear serialnumbers

disp('Reading swarm serialnumbers...');

%Create flag for serial number readout error
sn_err = 0;
%Loop trough all configured cassettes

for cn = 1:swarmcnt
    %Display the segment currently being analyzed
    disp(['    Segment ' segment_sn{cn} ':'])
    %Loop trough devices (addresses) for this cassette
    
    %Query address for serial number
    resp = swarm_read_serialnumber(ser,addr(cn),0);
    
    %Check if response is valid
    if(resp.valid)
        %If valid; Store serial number and inform user
        disp(['        S1 #' ': ' resp.aggregates.serialnumber]);
        serialnumber = resp.aggregates.serialnumber;
    else
        %If invalid, store as unknown, inform user and set error flag.
        serialnumber = 'Unknown';
        disp(['        S1 #'  ': Unknown (unable to read)']);
        pause(1)
        sn_err = 1;
    end

end

%If there was an error reading the serial number, this could indicate
%misconfiguration, bad connection etc. 
%Inform user of the error and ask if the script should continue.
if(sn_err)
    %Set answer key to a invalid character
    answ = 's';
    %While answer is invalid (not y or n). Ask user over and over again.
    while(lower(answ) ~= 'y' && lower(answ) ~= 'n')
        answ = input('Not all serialnumbers could be red. Do you want to continue anyway? (y/n)\n                                                                    ','s');
    end;
    %If user answerd n (no), abort script
    if strcmp(answ,'n')
        disp('Aborted by user...');
        return
    end;
end;
disp('Press any key to start the test...')
pause
disp('Starting test...');

%Store test start-time
starttime = datetime;

%Calculate estimated stop time and inform user
stoptime = datetime + hours(durH) + minutes(durM) + seconds(durS);
disp(['   Estimated time of completion at approx. ' datestr(stoptime + seconds(3)) '.']);
pause(3);

%Open/create output log file
fid = fopen([PathName '\' FileName], 'wt');
%Print log file legend
fprintf(fid,'%s, %s, %s, %s, %s, %s, %s, %s, %s\n','Timestamp','SWARM SN', 'RS-485 addr.', 'Type', 'Channel',  'Comm. status', 'Errormsg', 'Data, Thickness');

%While "now" is earlier than test stop time, loop.
while (datetime < stoptime)
    %Store start time of current iteration
    itStart = clock;
    %Loop trough all segments
    for cn = 1:swarmcnt
        %Inform user of segment currently being tested
        disp('---------------------------------------------------------------------------------');
        disp(['Testing segment #' num2str(cn) ' (SN: ' segment_sn{cn} '):'])
        disp('---------------------------------------------------------------------------------');
        %Get the time of test for this swarm device
        timestring = datestr(now);
        %Wrapping data aqusition in a try-catch to avvoid script
        %crashing due to matlab/windows events.
        try
            %Close serial port
            fclose(ser);
            %Prepare for messages of 16-bytes (no data packets)
            set(ser,'inputbuffer',16)
            set(ser,'timeout',2)
            %Start temperature data aquisition
            disp(['Performing temperature sensor test on Swarm S1 at address ' num2str(addr(cn)) '...']);
            %Measure external temperature on device
            %Flush serial port buffer
            flushinput(S)
            %Wait 200 milliseconds
            pause(0.2)
            %Clear response validity
            resp.valid = 0;
            %Set retry counter to zero
            retrycounter = 0;
            %Keep querying device until response is correct or max
            %retries is reached.
            while((resp.valid == 0 || isnan(resp.aggregates.temp)) && (retrycounter <= max_retries))
                if(retrycounter > 0)
                    %If this is not the first attempt, flush buffer and
                    %wait 200 ms.
                    flushinput(S)
                    pause(0.2)
                    %disp('.');
                end
                %Increment retry counter
                retrycounter = retrycounter + 1;
                %Send temperature data query for external (pipe) sensor to device
                resp = swarm_measure_temp_ext(S,addr(cn,j),0);
            end
            %If response was valid and value is not NaN..
            if(resp.valid && ~isnan(resp.aggregates.temp))
                %Collect the temperature value
                temp_ext = resp.aggregates.temp;
                %Decrement retrycounter (because the for-loop had added
                %one extra)
                retrycounter = retrycounter - 1;
                %Print result
                %If retries were needed, inform user.
                if(retrycounter > 0)
                    disp(['    External: Success!    [' num2str(temp_ext,'%.3f') '�C] (' num2str(retrycounter) ' retries)']);
                else
                    disp(['    External: Success!    [' num2str(temp_ext,'%.3f') '�C]']);
                end
                %Store result to log file
                fprintf(fid,'%s,%s,%s,%u,%s,%s,%u,%s,%.3f\n',timestring, segment_sn{cn}, serialnumbers{cn}, addr(cn), 'Temperature', 'Ext.',   resp.valid, cell2mat(resp.errormsg), temp_ext);
            elseif( resp.valid && isnan(resp.aggregates.temp))
                %If respone was invalid, set data to Nan
                temp_ext = resp.aggregates.temp;
                %Store result to log file
                fprintf(fid,'%s,%s,%s,%u,%s,%s,%u,%s,%.3f\n',timestring, segment_sn{cn}, serialnumbers{cn}, addr(cn), 'Temperature', 'Ext.',   1, cell2mat(resp.errormsg), temp_ext);
                %Inform user
                disp('    External: Response OK, but invalid temperature value. Sensor connected?');
                
            else
                %If respone was invalid, set data to Nan
                temp_ext = NaN;
                %Store result to log file
                fprintf(fid,'%s,%s,%s,%u,%s,%s,%u,%s,%.3f\n',timestring, segment_sn{cn}, serialnumbers{cn}, addr(cn), 'Temperature', 'Ext.',   0, cell2mat(resp.errormsg), temp_ext);
                %Inform user
                disp('    External: Error.');
                %Inform user of configured wait time
                disp(errorwaitmsg);
                %Wait
                pause(errorwaittime);
            end
                
