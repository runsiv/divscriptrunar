function varargout = Singleplot_swarm(varargin)
% SINGLEPLOT_SWARM MATLAB code for Singleplot_swarm.fig
%      SINGLEPLOT_SWARM, by itself, creates a new SINGLEPLOT_SWARM or raises the existing
%      singleton*.
%
%      H = SINGLEPLOT_SWARM returns the handle to a new SINGLEPLOT_SWARM or the handle to
%      the existing singleton*.
%
%      SINGLEPLOT_SWARM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SINGLEPLOT_SWARM.M with the given input arguments.
%
%      SINGLEPLOT_SWARM('Property','Value',...) creates a new SINGLEPLOT_SWARM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Singleplot_swarm_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Singleplot_swarm_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Singleplot_swarm

% Last Modified by GUIDE v2.5 03-Aug-2018 10:52:23

%clear;

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Singleplot_swarm_OpeningFcn, ...
                   'gui_OutputFcn',  @Singleplot_swarm_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before Singleplot_swarm is made visible.
function Singleplot_swarm_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Singleplot_swarm (see VARARGIN)

% Choose default command line output for Singleplot_swarm
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


delete(instrfind);
ser =ser_swarm;
setappdata(hObject.Parent, 'Serial', ser);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using Singleplot_swarm.
if strcmp(get(hObject,'Visible'),'off')
    %plot(rand(5));
    resp = read_swarm(ser);
    plot(resp.aggregates.waveform)
end

% UIWAIT makes Singleplot_swarm wait for user response (see UIRESUME)
% uiwait(handles.figure1);

function [ser] = setup_serial()

    ser = serial('COM7', 'BaudRate', 115200, 'inputbuffer',1520);

% --- Outputs from this function are returned to the command line.
function varargout = Singleplot_swarm_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
cla;
ser = getappdata(hObject.Parent, 'Serial');
popup_sel_index = get(handles.popupmenu1, 'Value');
switch popup_sel_index
    case 1
        
        % resp = swarm_measure_us(ser,7, 0, 1);
        resp = read_swarm(ser);
        plot(resp.aggregates.waveform)
    case 2
        plot(sin(1:0.01:25.99));
    case 3
        bar(1:.5:10);
    case 4
        plot(membrane);
    case 5
        surf(peaks);
end

function [resp] = read_swarm(ser)
    resp = swarm_measure_us(ser,7, 0, 1);





% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'plot(rand(5))', 'plot(sin(1:0.01:25))', 'bar(1:.5:10)', 'plot(membrane)', 'surf(peaks)'});


% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
 
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes1


% --- Executes on mouse press over axes background.
function axes1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
