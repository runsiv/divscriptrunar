% create a matlab struct with all the files in the folder%
clear;
%cd Testdata\
dir_info = dir('*.tar.gz');
% The data shall be organaized into two layers, 
% the top layer will contain all the different sensors (hopefully)
% identified by it's serial number combined with the position data
% the second layer will contain the following data:
%Timestamp, delay, temp_pipe, min_s, min_thick, frequency, uniqeID(SN), posistion(row/col for swarm and uniqeID for ultramonit), trace, backwall_pos (pos and A), algortimeversio
% the inputs to the shot should be seperated into a different file


dataset_all_measurements = struct;
sensor = struct('uniqeID', 0, 'data', 0);
data = struct;

for i = 1:length(dir_info)
    untar(dir_info(i).name)
    dir_info(i).name
    

    cd tmp/ultramonit/out
    
    dir_info_tmp = dir('*um_raw*');
    
    jsondata = struct;
    
    
    for j = 1 : length(dir_info_tmp)
        
       jsondata(j).sensor = jsondecode(fileread(dir_info_tmp(j).name)); % read the json file
       sensor = strcat(jsondata(j).sensor.controller_name, '_',  jsondata(j).sensor.measurements.uniqueID);
       data(1).(sensor)(i).timestamp = jsondata(j).sensor.daytime;
       data(1).(sensor)(i).temperature = jsondata(j).sensor.measurements.temp_pipe;
       data(1).(sensor)(i).trace = jsondata(j).sensor.measurements.trace;
       data(1).(sensor)(i).min_s = jsondata(j).sensor.measurements.minS;
       data(1).(sensor)(i).min_thickness = jsondata(j).sensor.measurements.min_th;
       data(1).(sensor)(i).frequency = jsondata(j).sensor.measurements.frequency;

       
      
    end
  
    
        
 
    
    cd ..
     cd ..
    cd ..
    rmdir('tmp', 's')
    i;
end
% prompt  = 'Enter name of the dataset:' ;
%filename = input(prompt, 's');
%save (filename ,'struct','dataset_all_measurements.mat')