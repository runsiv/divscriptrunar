load 'um_protocol.mat'

%Clear potentially remaining variable from previous runs (other matrix
%sizes could cause confusion and error
clear addr;

%Set the time to wait after an error before performing the next data
%query. This is to ensure that an error in the swarm, causing a device
%reset, will not effect the next query. For swarm, the time from crash to
%full functionality is regains is maximum 13 seconds (as of fw v0.9.0).
errorwaittime = 13;
%Error message to desplay
errorwaitmsg = ['Error detected. Waiting ' num2str(errorwaittime) ' seconds for system to stabilize.'];

%Have user input the desired communication port (Windows COMPORT)
comport = input('Input Serial port (e.g. "COM2"):\n','s');

%Have user input the desired number of segments to test
cassno = str2num(input('Please enter number of segments to test:\n','s'));

%Ask user to verify the configuration
answ = 'k';
while(lower(answ) ~= 'y' && lower(answ) ~= 'n')
    answ = input('Do you want acceleration to be collected during the test? (y/n):\n','s');
end;
%If configuration is selected as invalid, abort the script
if strcmp(lower(answ),'n')
    disp('	No acceleration will be collected.');
    acc_enable = 0;
else
    disp('	Acceleration will be collected.');
    acc_enable = 1;
end;
pause(1);
%Loop trough the inputted number of cassettes and gather information
for cn = 1:cassno
    %Have user input the serial number of cassette no 'cn'
    segment_sn{cn} = input(['Enter serialnumber of segment #' num2str(cn) ':\n'],'s');
    
    %Query user to input the RS-485 address of the first swarm S1M16
    addr(cn,1) = str2num(input(['   Segment #' num2str(cn) ': Address of Swarm S1M16 #1:\n'],'s'));
    addr(cn,2) = str2num(input(['   Segment #' num2str(cn) ': Address of Swarm S1M16 #2:\n'],'s'));
    addr(cn,3) = str2num(input(['   Segment #' num2str(cn) ': Address of Swarm S1M16 #3:\n'],'s'));
     addr(cn,4) = str2num(input(['   Segment #' num2str(cn) ': Address of Swarm S1M16 #3:\n'],'s'));
end;

%Print out the cassette/swarm/address configuration to the user
disp('');
disp('');
disp('');
disp('Configuration is:');

for cn = 1:cassno
    disp(['    Segment ' segment_sn{cn} ':']);
    disp(['         S1M16 #1 addr.: ' num2str(addr(cn,1))]);
    disp(['         S1M16 #2 addr.: ' num2str(addr(cn,2))]);
    disp(['         S1M16 #3 addr.: ' num2str(addr(cn,3))]);
     disp(['         S1M16 #4 addr.: ' num2str(addr(cn,4))]);
end;

%Ask user to verify the configuration
answ = 'k';
while(lower(answ) ~= 'y' && lower(answ) ~= 'n')
    answ = input('Verify that this configuration is correct (y/n):\n','s');
end;
%If configuration is selected as invalid, abort the script
if strcmp(answ,'n')
    disp('Test script aborted by user...');
    return
end;
disp('Configuration confirmed by user...');
disp('');

%Checking that none of the configured addresses are the same as this
%scripts address
if(addr(addr == ultramonit_protocol.own_address))
    disp(['One of the configured devices has the same RS485-address as this script (' num2str(ultramonit_protocol.own_address) ').'])
    disp('Finding available address');
    new_selfaddr = ultramonit_protocol.own_address;
    while(addr(addr == new_selfaddr))
        new_selfaddr = round(rand()*254);
        fprintf('.');
    end;
    disp(['Changing address of this script to ' num2str(new_selfaddr) '.']);
    ultramonit_protocol.own_address = new_selfaddr;
end;
%Have user input the desired test duration
duration = strsplit(input('Enter the desired test duration (HH:MM:SS):\n','s'),':');
%Convert duration string to hours, minutes and seconds
durH = str2num(cell2mat(duration(1)));
durM = str2num(cell2mat(duration(2)));
durS = str2num(cell2mat(duration(3)));

%Have user input the desired iteration interval. This is the interval between
%each full iteration of all devices. E.g. If the test starts at 14:05:00
%and the interval is set to 120 seconds. The next iteration will begin at
%14:07:00. If the interval is shorter than one full system test iteration,
%the test will run continously
delay = str2num(input('Please enter interval between each full iteration: [s]\n','s'));


%Set the maximum allowed number of retries (attempts to query the
%device if the first attempt fails). Minimum value is 1
%max_retries = str2num(input('Enter the maximum allowed retries per data query. Minimum value is 1.'));
%if(max_retries < 1)
    max_retries = 3;
%end;
disp(['Max retries set to ' num2str(max_retries) '.']);

%Create the 'default' file name for output file.
fname = 'cgcm_segment_log.txt';
%Have user select the desired output file name and location
[FileName,PathName] = uiputfile('.txt','Name of output file(s)',fname);


%Set the baudrate for the serial communication
baud = 115200;
%Set the timeout for the serialport. This should be quite high if e.g.
%'averages' is configured higly on swarm, or other 'measurement time
%affeting' settings are configured.
timeout = 3.5;

%datalength = zeros(1,numel(addr));


%Handle serialport opening
%---
%Check if a variable called 'S' already exist (possibly from previous test
%runs).
if(exist('S'))
    %If 's' exists, check if it is a serial port object.
    if(strcmp(class(S),'serial'))
        %If it is a serial port object; Try to close is
        try
            fclose(S);
        catch
            %If closing 'S' fails, clear all 'instruments' from matlab
            %memory.
            fclose(instrfindall);
            delete(instrfindall);
            %Recreate 'S' with the correct baud rate
            S = serial(comport,'baud',baud);
        end
    else
        %If 'S' is not a serial port object, discard the variable
        clear S;
        %Create 'S' as serial port object with the correct baud rate
        S = serial(comport,'baud',baud);
    end;

end;

%Try to open the serial port
try
    fopen(S);
catch
    %If serial port 'S' could not be opened...
    disp('COM-port not available. Forcing.');
    %Flush all knowns instruments (ports) from matlab memory
    %fclose(instrfindall);
    delete(instrfindall);
    %Recreate port
    S = serial(comport,'baud',baud);
    
end;

%---
%Collecting serial numbers digitally
%---
%Close serial port
fclose(S);
%Configure COM port for messages of 50-bytes
set(S,'inputbuffer',28);
%Set timeout to 2 seconds
set(S,'timeout',2);
%Clear any remaining serial numbers from potential previous test runs
clear serialnumbers

disp('Reading swarm serialnumbers...');

%Create flag for serial number readout error
sn_err = 0;
%Loop trough all configured cassettes
for cn = 1:cassno
    %Display the segment currently being analyzed
    disp(['    Segment ' segment_sn{cn} ':'])
    %Loop trough devices (addresses) for this cassette
    for a = 1:1
        flushinput(S);
        %Query address for serial number
        resp = swarm_read_serialnumber(S,addr(cn, a),0);
        
        %Check if response is valid
        if(resp.valid)
            %If valid; Store serial number and inform user
            disp(['        S1M16 #' num2str(a) ': ' resp.aggregates.serialnumber]);
            serialnumbers{cn,a} = resp.aggregates.serialnumber;
        else
            %If invalid, store as unknown, inform user and set error flag.
            serialnumbers{cn,a} = 'Unknown';
            disp(['        S1M16 #' num2str(a) ': Unknown (unable to read)']);
            pause(1)
            sn_err = 1;
        end;
    end;
end;

%If there was an error reading the serial number, this could indicate
%misconfiguration, bad connection etc. 
%Inform user of the error and ask if the script should continue.
if(sn_err)
    %Set answer key to a invalid character
    answ = 's';
    %While answer is invalid (not y or n). Ask user over and over again.
    while(lower(answ) ~= 'y' && lower(answ) ~= 'n')
        answ = input('Not all serialnumbers could be red. Do you want to continue anyway? (y/n)\n                                                                    ','s');
    end;
    %If user answerd n (no), abort script
    if strcmp(answ,'n')
        disp('Aborted by user...');
        return
    end;
end;
disp('Press any key to start the test...')
pause
disp('Starting test...');

%Store test start-time
starttime = datetime;

%Calculate estimated stop time and inform user
stoptime = datetime + hours(durH) + minutes(durM) + seconds(durS);
disp(['   Estimated time of completion at approx. ' datestr(stoptime + seconds(3)) '.']);
pause(3);

%Open/create output log file
fid = fopen([PathName '\' FileName], 'wt');
%Print log file legend
fprintf(fid,'%s, %s, %s, %s, %s, %s, %s, %s, %s\n','Timestamp','Segment SN', 'S1M16 SN','RS-485 addr.', 'Type', 'Channel',  'Comm. status', 'Errormsg', 'Data');


%While "now" is earlier than test stop time, loop.
while (datetime < stoptime)
    %Store start time of current iteration
    itStart = clock;
    %Loop trough all segments
    for cn = 1:cassno
        %Inform user of segment currently being tested
        disp('---------------------------------------------------------------------------------');
        disp(['Testing segment #' num2str(cn) ' (SN: ' segment_sn{cn} '):'])
        disp('---------------------------------------------------------------------------------');
        
        %Loop trough swarm s1m16 devices
        for j=1:1

            %Get the time of test for this swarm device
            timestring = datestr(now);
            %Wrapping data aqusition in a try-catch to avvoid script
            %crashing due to matlab/windows events.
            try
                
                %Close serial port
                fclose(S);
                %Prepare for messages of 16-bytes (no data packets)
                set(S,'inputbuffer',16)
                set(S,'timeout',2)
                if(acc_enable)
                    %----------------------------------------------------------------------------------------------------------------------
                    %Start acceleration data aquisition
                    disp(['Performing accelerometer test on Swarm S1M16 at address ' num2str(addr(cn,j)) '...']);
                    %Flush serial port buffer
                    flushinput(S)
                    %Wait 200 milliseconds
                    pause(0.2)
                    %Clear response validity
                    resp.valid = 0;
                    %Set retry counter to zero
                    retrycounter = 0;
                    %Keep querying device until response is correct or max
                    %retries is reached.
                    while(resp.valid == 0 && retrycounter <= max_retries)
                        %If this is not the first attempt, flush buffer and
                        %wait 200 ms.
                        if(retrycounter > 0)
                            flushinput(S)
                            pause(0.2)
                            %disp('.');
                        end;
                        %Increment retry counter
                        retrycounter = retrycounter + 1;
                        %Send acceleration data query for X-axis to device
                        resp = swarm_measure_acc_x(S,addr(cn,j),0);
                    end;
                    %If response was valid..
                    if(resp.valid)
                        %Collect the acceleration value
                        accx = resp.aggregates.acc;
                        %Decrement retrycounter (because the for-loop had added
                        %one extra)
                        retrycounter = retrycounter - 1;
                        %Print result
                        %If retries were needed, inform user.
                        if(retrycounter > 0)
                            disp(['    X-axis: Success!      [' num2str(accx,'%+1.4f') 'g](' num2str(retrycounter) ' retries)']);
                        else
                            disp(['    X-axis: Success!      [' num2str(accx,'%+1.4f') 'g]']);
                        end;   
                        %Store result to log file
                        fprintf(fid,'%s,%s,%s,%u,%s,%s,%u,%s,%.3f\n',timestring, segment_sn{cn}, serialnumbers{cn,j}, addr(cn,j), 'Acceleration', 'X',   resp.valid, cell2mat(resp.errormsg), accx);
                    else
                        %If respone was invalid, set data to Nan
                        accx = NaN;
                        %Inform user
                        disp('    X-axis: Error.');
                        %Store result to log file
                        fprintf(fid,'%s,%s,%s,%u,%s,%s,%u,%s,%.3f\n',timestring, segment_sn{cn}, serialnumbers{cn,j}, addr(cn,j), 'Acceleration', 'X',   resp.valid, cell2mat(resp.errormsg), accx);
                        %Inform user of configured wait time
                        disp(errorwaitmsg);
                        %Wait
                        pause(errorwaittime);
                    end;
                    %Flush serial port buffer
                    flushinput(S)
                    %Wait 200 milliseconds
                    pause(0.2)
                    %Clear response validity
                    resp.valid = 0;
                    %Set retry counter to zero
                    retrycounter = 0;
                    %Keep querying device until response is correct or max
                    %retries is reached.
                    while(resp.valid == 0 && retrycounter <= max_retries)
                        %If this is not the first attempt, flush buffer and
                        %wait 200 ms.
                        if(retrycounter > 0)
                            flushinput(S)
                            pause(0.2)
                            %disp('.');
                        end;
                        %Increment retry counter
                        retrycounter = retrycounter + 1;
                        %Send acceleration data query for Y-axis to device
                        resp = swarm_measure_acc_y(S,addr(cn, j),0);
                    end;
                    %If response was valid..
                    if(resp.valid)
                        %Collect the acceleration value
                        accy = resp.aggregates.acc;
                        %Decrement retrycounter (because the for-loop had added
                        %one extra)
                        retrycounter = retrycounter - 1;
                        %Print result
                        %If retries were needed, inform user.
                        if(retrycounter > 0)
                            disp(['    Y-axis: Success!      [' num2str(accy,'%+1.4f') 'g](' num2str(retrycounter) ' retries)']);
                        else
                            disp(['    Y-axis: Success!      [' num2str(accy,'%+1.4f') 'g]']);
                        end;                    
                        %Store result to log file
                        fprintf(fid,'%s,%s,%s,%u,%s,%s,%u,%s,%.3f\n',timestring, segment_sn{cn}, serialnumbers{cn, j}, addr(cn, j), 'Acceleration', 'Y',   resp.valid, cell2mat(resp.errormsg), accy);
                    else
                        %If respone was invalid, set data to Nan
                        accy = NaN;
                        %Inform user
                        disp('    Y-axis: Error.');
                        %Store result to log file
                        fprintf(fid,'%s,%s,%s,%u,%s,%s,%u,%s,%.3f\n',timestring, segment_sn{cn}, serialnumbers{cn, j}, addr(cn, j), 'Acceleration', 'Y',   resp.valid, cell2mat(resp.errormsg), accy);
                        %Inform user of configured wait time
                        disp(errorwaitmsg);
                        %Wait
                        pause(errorwaittime);
                    end;
                    %Flush serial port buffer
                    flushinput(S)
                    %Wait 200 milliseconds
                    pause(0.2)
                    %Clear response validity
                    resp.valid = 0;
                    %Set retry counter to zero
                    retrycounter = 0;
                    %Keep querying device until response is correct or max
                    %retries is reached.
                    while(resp.valid == 0 && retrycounter <= max_retries)
                        %If this is not the first attempt, flush buffer and
                        %wait 200 ms.
                        if(retrycounter > 0)
                            flushinput(S)
                            pause(0.2)
                            %disp('.');
                        end;
                        %Increment retry counter
                        retrycounter = retrycounter + 1;
                        %Send acceleration data query for Z-axis to device
                        resp = swarm_measure_acc_z(S,addr(cn, j),0);
                    end;
                    %If response was valid..
                    if(resp.valid)
                        %Collect the acceleration value
                        accz = resp.aggregates.acc;
                        %Decrement retrycounter (because the for-loop had added
                        %one extra)
                        retrycounter = retrycounter - 1;
                        %Print result
                        %If retries were needed, inform user.
                        if(retrycounter > 0)
                            disp(['    Z-axis: Success!      [' num2str(accz,'%+1.4f') 'g] (' num2str(retrycounter) ' retries)']);
                        else
                            disp(['    Z-axis: Success!      [' num2str(accz,'%+1.4f') 'g]']);
                        end;
                        %Store result to log file
                        fprintf(fid,'%s,%s,%s,%u,%s,%s,%u,%s,%.3f\n',timestring, segment_sn{cn}, serialnumbers{cn, j}, addr(cn, j), 'Acceleration', 'Z',   resp.valid, cell2mat(resp.errormsg), accz);
                    else
                        %If respone was invalid, set data to Nan
                        accz = NaN;
                        %Inform user
                        disp('    Z-axis: Error.');
                        %Store result to log file
                        fprintf(fid,'%s,%s,%s,%u,%s,%s,%u,%s,%.3f\n',timestring, segment_sn{cn}, serialnumbers{cn, j}, addr(cn, j), 'Acceleration', 'Z',   resp.valid, cell2mat(resp.errormsg), accz);
                        %Inform user of configured wait time
                        disp(errorwaitmsg);
                        %Wait
                        pause(errorwaittime);
                    end;
                end;
				%----------------------------------------------------------------------------------------------------------------------
				%Start temperature data aquisition
                disp(['Performing temperature sensor test on Swarm S1M16 at address ' num2str(addr(cn, j)) '...']);
                %Measure external temperature on device 3
               
				%Flush serial port buffer
                flushinput(S)
				%Wait 200 milliseconds
                pause(0.2)
				%Clear response validity
                resp.valid = 0;
				%Clear old value (if present)				
                resp.aggregates.temp = NaN;
				%Set retry counter to zero
                retrycounter = 0;
				%Keep querying device until response is correct or max
				%retries is reached.
                while((resp.valid == 0 || isnan(resp.aggregates.temp)) && (retrycounter <= max_retries))
                    if(retrycounter > 0)
                        %If this is not the first attempt, flush buffer and
						%wait 200 ms.
						flushinput(S)
                        pause(0.2)
                        %disp('.');
                    end;
					%Increment retry counter
                    retrycounter = retrycounter + 1;
					%Send temperature data query for internal sensor to device
                    resp = swarm_measure_temp_int(S,addr(cn, j),0);
                end;
				%If response was valid and value is not NaN..
                if(resp.valid)
					%Collect the temperature value
                    temp_int = resp.aggregates.temp;
					%Decrement retrycounter (because the for-loop had added
					%one extra)
                    retrycounter = retrycounter - 1;
					%Print result
					%If retries were needed, inform user.
                    if(retrycounter > 0)
                        disp(['    Internal: Success!    [' num2str(temp_int,'%.3f') '?C](' num2str(retrycounter) ' retries)']);
                    else
                        disp(['    Internal: Success!    [' num2str(temp_int,'%.3f') '?C]']);
                    end;
                    %Store result to log file
                    fprintf(fid,'%s,%s,%s,%u,%s,%s,%u,%s,%.3f\n',timestring, segment_sn{cn}, serialnumbers{cn, j}, addr(cn, j), 'Temperature', 'Int.',   resp.valid, cell2mat(resp.errormsg), temp_int);
                else
                    %If respone was invalid, set data to Nan
                    temp_int = NaN;
					%Store result to log file
                    fprintf(fid,'%s,%s,%s,%u,%s,%s,%u,%s, %s, %.3f\n',timestring, segment_sn{cn}, serialnumbers{cn, j}, addr(cn, j), 'Temperature', 'Int.',   resp.valid, cell2mat(resp.errormsg), temp_int);
                    %Inform user
					disp('    Internal: Error.');
					%Inform user of configured wait time
                    disp(errorwaitmsg);
					%Wait
                    pause(errorwaittime);
                end;
            catch
				%Display error message
				disp('Unexpected error with test program. Crashed during acceleration/temperature data aqusition.');
            end
			%----------------------------------------------------------------------------------------------------------------------
			%Start Ultrasound data aquistion
            disp(['Performing ultrasound test on Swarm S1M16 at address ' num2str(addr(cn, j)) '...']);
            %Close serial port
			fclose(S);
			%Set input buffer to 1520 to prepare for long ultrasound data packets
            set(S,'inputbuffer',1520)
			%Set timeout to 5 to account for large averageing settings
            set(S,'timeout',5)
			%Check cassette type:
            if j == 1
                %If device number is 1, only measure ultrasound on the first 10 channels
                nc = 2;
            elseif j == 2    
                %If device number is 2, only measure ultrasound on the first 15 channels
				nc = 0;
            elseif j == 3
				%If device number is 3, only measure ultrasound on the first 9 channels
                nc = 0;
            elseif j == 4
                nc = 0;
            else
                error('Device number exceeds 3. This is a fatal error.');
            end;
            %Loop trough ultrasound channels
            for c = 1:nc
                %Wrap in try-catch to prevent script stopping from unexpected events (windows/matlab etc.)
				try
					%Clear response validity
                    resp.valid = 0;
					%Reset retry counter
                    retrycounter = 0;
					%Keep querying device until response is correct or max
					%retries is reached.
                    while(resp.valid == 0 && retrycounter <= max_retries)
                        if(retrycounter > 0)
							%If this is not the first attempt, flush buffer and
							%wait 200 ms.
                            flushinput(S)
                            pause(0.2)
                            %disp('.');
                        end;
						%Increment retry counter
                        retrycounter = retrycounter + 1;
						%Send ultrasound data query for channel 'c' to device
                        resp = swarm_measure_us(S,addr(cn, j),c,0);
                    end;
					
                    if(resp.valid == 1 && isfield(resp,'aggregates') && isfield(resp.aggregates,'waveform'))
                        %If response was valid; collect ultrasound data
						data = resp.aggregates.waveform;
                    else
						%If response was invalid; Clear ultrasound data
                        data = [];
                    end;
					%Check if response is structure (to avvoid matlab crashing in case it isn't.)
                    if(isstruct(resp))
                        if(resp.valid ~= 1)
							%If there was an error, notify user. Store result to log file and wait (if errorwaittime is configured).
                            disp(['    Channel ' num2str(c) ': Error.']);
                            fprintf(fid,'%s,%s,%s,%u,%s,%u,%u,%s,%s\n',timestring, segment_sn{cn}, serialnumbers{cn, j}, addr(cn, j), 'Ultrasound', c,   resp.valid, cell2mat(resp.errormsg), '');
                            disp(errorwaitmsg);
                            pause(errorwaittime);
                            flushinput(S);                      
                        else
							%If measurement was successful..
							%Store result to log file
                            fprintf(fid,'%s,%s,%s,%u,%s,%u,%u,%s,%s,%s\n',timestring, segment_sn{cn}, serialnumbers{cn, j}, addr(cn, j), 'Ultrasound', c,   resp.valid, cell2mat(resp.errormsg), ['[' num2str(data) ']'], num2str(data));
                            %Decrement retrycounter (because the for-loop had added
							%one extra)
							retrycounter = retrycounter - 1;
							%Print result
							%If retries were needed, inform user.
                            if(retrycounter > 0)
                                disp(['    Channel ' num2str(c) ': Success! (' num2str(retrycounter) ' retries)']);
                            else
                                disp(['    Channel ' num2str(c) ': Success!']);
                            end;
                        end;
                    else
						%Print out error message
                        disp(['    Channel ' num2str(c) ': Testscript error.)']);
                        pause(2);
                        flushinput(S);
                    end;

                catch
					%Catch any unexpected errors during ultrasound aquisition
                    disp(['    Channel ' num2str(c) ': Script error.']);
                    errormsg = {'Reading COM-port chrashed." Flushing COM-Port and continuing test.'};
                    pause(2);
                    flushinput(S);
                    fprintf(fid,'%s,%s,%u,%s,%u,%u,%s,%s,%f\n',timestring,segment_sn{cn}, serialnumbers{cn, j}, addr(cn, j),'Ultrasound',c, 0, cell2mat(errormsg),0);

                end
                flushinput(S);
            end
        end;
    end;
	%Declare variable for holding actual delay time between iterations
    delay_act = 0;
	%Calculate elapsed time during previous iterations
    elapsed = etime(clock, itStart);
	%If delay was set to non-zero by user
    if(delay)
		%Check if elapsed time is positive and shorter than the user-given delay
       if((elapsed > 0) && (elapsed < delay))
			%Set actual delay to user-given delay - elapsed time
            delay_act = (delay - elapsed);
			%Prevent negative delay (time travel?)
            if(delay_act < 0)
                delay_act = 0;
            end;
		%If elapsed time is longer than the user-given delay, start next iteration immidiately
        elseif(elapsed > 0)
            delay_act = 0;
        end;
    end;
	%Notify user that the current test iteration is complete
    disp('---------------------------------------------------------------------------------');
    disp('---------------------------------------------------------------------------------');
    disp('Test iteration complete!');
	%Display iteration duration. Good to know for planning future tests.
    disp(['This iteration took ' num2str(elapsed) ' seconds.']);
	%Check if, after the current planned delay, the test will end. If not, then notify user of the time until next iteration
    if(delay_act && datetime < (stoptime - seconds(delay_act)))
        disp(['Test will continue in ' num2str(round(delay_act)) ' seconds, based on configured test iteration interval.']);
    end;
	%If this was not the last iteration
	if(datetime < (stoptime - seconds(delay_act)))
		%Display esitmated remaining time of test
		%If test will last for over one day, print with D: formatting. If not, only print HH:MM:SS.
		if(datetime(stoptime)) > (datetime('now') + days(1))
			disp(['Total estimated remaining test time: ' char(((stoptime) - now),'D:HH:mm:ss') ]);
		else
			disp(['Total estimated remaning test time: ' char(((stoptime) - now),'HH:mm:ss') ]);
		end
		%Wait for next iteration to start if delay is needed
		if(delay_act)
			pause(delay_act);
		end;
	end;
	
end;
disp('');
disp('---------------------------------------------------------------------------------');
disp('---------------------------------------------------------------------------------');
disp('---------------------------------------------------------------------------------');
disp('---------------------------------------------------------------------------------');
disp('Test fully completed!');
disp('');
disp('Log file can be found here:');
disp([PathName FileName])
fclose(S);
fclose(fid);
fclose('all');

