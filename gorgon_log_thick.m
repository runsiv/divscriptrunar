load 'um_protocol.mat'

%Clear potentially remaining variable from previous runs (other matrix
%sizes could cause confusion and error
clear addr;

%Set the time to wait after an error before performing the next data
%query. This is to ensure that an error in the swarm, causing a device
%reset, will not effect the next query. For swarm, the time from crash to
%full functionality is regains is maximum 13 seconds (as of fw v0.9.0).
errorwaittime = 0;
%Error message to desplay
errorwaitmsg = ['Error detected. Waiting ' num2str(errorwaittime) ' seconds for system to stabilize.'];

%Have user input the desired communication port (Windows COMPORT)
comport = input('Input Serial port (e.g. "COM2"):\n','s');

%Have user input the desired number of segments to test
cassno = str2num(input('Please enter number of segments to test:\n','s'));

%Ask user to verify the configuration
answ = 'k';
while(lower(answ) ~= 'y' && lower(answ) ~= 'n')
    answ = input('Do you want acceleration to be collected during the test? (y/n):\n','s');
end;
%If configuration is selected as invalid, abort the script
if strcmp(lower(answ),'n')
    disp('	No acceleration will be collected.');
    acc_enable = 0;
else
    disp('	Acceleration will be collected.');
    acc_enable = 1;
end;
pause(1);
%Loop trough the inputted number of cassettes and gather information
for cn = 1:cassno
    %Have user input the serial number of cassette no 'cn'
    segment_sn{cn} = input(['Enter serialnumber of segment #' num2str(cn) ':\n'],'s');
    
    %Query user to input the RS-485 address of the first swarm S1M16
    addr(cn,1) = str2num(input(['   Segment #' num2str(cn) ': Address of Swarm S1M16 #1:\n'],'s'));
    addr(cn,2) = str2num(input(['   Segment #' num2str(cn) ': Address of Swarm S1M16 #2:\n'],'s'));
%     addr(cn,3) = str2num(input(['   Segment #' num2str(cn) ': Address of Swarm S1M16 #3:\n'],'s'));
end;

%Print out the cassette/swarm/address configuration to the user
disp('');
disp('');
disp('');
disp('Configuration is:');

for cn = 1:cassno
    disp(['    Segment ' segment_sn{cn} ':']);
    disp(['         S1M16 #1 addr.: ' num2str(addr(cn,1))]);
    disp(['         S1M16 #2 addr.: ' num2str(addr(cn,2))]);
%     disp(['         S1M16 #3 addr.: ' num2str(addr(cn,3))]);
end;

%Ask user to verify the configuration
answ = 'k';
while(lower(answ) ~= 'y' && lower(answ) ~= 'n')
    answ = input('Verify that this configuration is correct (y/n):\n','s');
end;
%If configuration is selected as invalid, abort the script
if strcmp(answ,'n')
    disp('Test script aborted by user...');
    return
end;
disp('Configuration confirmed by user...');
disp('');

%Checking that none of the configured addresses are the same as this
%scripts address
if(addr(addr == ultramonit_protocol.own_address))
    disp(['One of the configured devices has the same RS485-address as this script (' num2str(ultramonit_protocol.own_address) ').'])
    disp('Finding available address');
    new_selfaddr = ultramonit_protocol.own_address;
    while(addr(addr == new_selfaddr))
        new_selfaddr = round(rand()*254);
        fprintf('.');
    end;
    disp(['Changing address of this script to ' num2str(new_selfaddr) '.']);
    ultramonit_protocol.own_address = new_selfaddr;
end;
%Have user input the desired test duration
duration = strsplit(input('Enter the desired test duration (HH:MM:SS):\n','s'),':');
%Convert duration string to hours, minutes and seconds
durH = str2num(cell2mat(duration(1)));
durM = str2num(cell2mat(duration(2)));
durS = str2num(cell2mat(duration(3)));

%Have user input the desired iteration interval. This is the interval between
%each full iteration of all devices. E.g. If the test starts at 14:05:00
%and the interval is set to 120 seconds. The next iteration will begin at
%14:07:00. If the interval is shorter than one full system test iteration,
%the test will run continously
delay = str2num(input('Please enter interval between each full iteration: [s]\n','s'));


%Set the maximum allowed number of retries (attempts to query the
%device if the first attempt fails). Minimum value is 1
%max_retries = str2num(input('Enter the maximum allowed retries per data query. Minimum value is 1.'));
%if(max_retries < 1)
    max_retries = 0;
%end;
disp(['Max retries set to ' num2str(max_retries) '.']);

%Create the 'default' file name for output file.
fname = 'cgcm_segment_log.mat';
%Have user select the desired output file name and location
[FileName,PathName] = uiputfile('.mat','Name of output file(s)',fname);


%Set the baudrate for the serial communication
baud = 115200;
%Set the timeout for the serialport. This should be quite high if e.g.
%'averages' is configured higly on swarm, or other 'measurement time
%affeting' settings are configured.
timeout = 3.5;

%datalength = zeros(1,numel(addr));
%Handle serialport opening
%---
%Check if a variable called 'S' already exist (possibly from previous test
%runs).
if(exist('ser'))
    %If 's' exists, check if it is a serial port object.
    if(strcmp(class(ser),'serial'))
        %If it is a serial port object; Try to close is
        try
            fclose(ser);
        catch
            %If closing 'S' fails, clear all 'instruments' from matlab
            %memory.
            fclose(instrfindall);
            delete(instrfindall);
            %Recreate 'S' with the correct baud rate
            ser = serial(comport,'baud',baud);
        end
    else
        %If 'S' is not a serial port object, discard the variable
        clear S;
        %Create 'S' as serial port object with the correct baud rate
        ser = serial(comport,'baud',baud);
    end

end

%Try to open the serial port
try
    fopen(ser);
catch
    %If serial port 'S' could not be opened...
    disp('COM-port not available. Forcing.');
    %Flush all knowns instruments (ports) from matlab memory
    %fclose(instrfindall);
    delete(instrfindall);
    %Recreate port
    ser = serial(comport,'baud',baud);
    
end
%---
%Collecting serial numbers digitally
%---
%Close serial port
fclose(S);
%Configure COM port for messages of 50-bytes
set(S,'inputbuffer',28);
%Set timeout to 2 seconds
set(S,'timeout',2);
%Clear any remaining serial numbers from potential previous test runs
clear serialnumbers

disp('Reading swarm serialnumbers...');
%Create flag for serial number readout error
sn_err = 0;
%Loop trough all configured cassettes
for cn = 1:cassno
    %Display the segment currently being analyzed
    disp(['    Segment ' segment_sn{cn} ':'])
    %Loop trough devices (addresses) for this cassette
    for a = 1:3
        %Query address for serial number
        resp = swarm_read_serialnumber(S,addr(cn, a),0);
        
        %Check if response is valid
        if(resp.valid)
            %If valid; Store serial number and inform user
            disp(['        S1M16 #' num2str(a) ': ' resp.aggregates.serialnumber]);
            serialnumbers{cn,a} = resp.aggregates.serialnumber;
        else
            %If invalid, store as unknown, inform user and set error flag.
            serialnumbers{cn,a} = 'Unknown';
            disp(['        S1M16 #' num2str(a) ': Unknown (unable to read)']);
            pause(1)
            sn_err = 1;
        end
    end
end


%If there was an error reading the serial number, this could indicate
%misconfiguration, bad connection etc. 
%Inform user of the error and ask if the script should continue.
if(sn_err)
    %Set answer key to a invalid character
    answ = 's';
    %While answer is invalid (not y or n). Ask user over and over again.
    while(lower(answ) ~= 'y' && lower(answ) ~= 'n')
        answ = input('Not all serialnumbers could be red. Do you want to continue anyway? (y/n)\n                                                                    ','s');
    end;
    %If user answerd n (no), abort script
    if strcmp(answ,'n')
        disp('Aborted by user...');
        return
    end
end
disp('Press any key to start the test...')
pause
disp('Starting test...');

%Store test start-time
starttime = datetime;

%Calculate estimated stop time and inform user
stoptime = datetime + hours(durH) + minutes(durM) + seconds(durS);
disp(['   Estimated time of completion at approx. ' datestr(stoptime + seconds(3)) '.']);
pause(3);
sensor_settings = struct;
sensor_settings.Xintp = 100;
sensor_settings.max_jump =  0.2;
sensor_settings.temperatureCoefficient = 1.05e-04;
sensor_settings.tempetatureOffset = 25;

sensor = struct;
sensor.minS = 1000;
sensor.min_th = 20;
sensor.frequency = 25;
sensor.speedOfSound = 5920;
sensor.Xintp = 100;
sensor.delay_th = 22;

while (datetime < stoptime)
     %Store start time of current iteration
    itStart = clock;
    %Loop trough all segments
      for cn = 1:cassno
        %Inform user of segment currently being tested
        disp('---------------------------------------------------------------------------------');
        disp(['Testing segment #' num2str(cn) ' (SN: ' segment_sn{cn} '):'])
        disp('---------------------------------------------------------------------------------');
        fclose(S);
                %Prepare for messages of 16-bytes (no data packets)
                set(S,'inputbuffer',16)
                set(S,'timeout',2)
        %Loop trough swarm s1m16 devices
        for j=1:35
                 %Get the time of test for this swarm device
            timestring = datestr(now);
            %Wrapping data aqusition in a try-catch to avvoid script
            %crashing due to matlab/windows events.
                    if j <=10
                    k = 1;
                    tr_cnt = 0;
                elseif j>10 && j<=25
                    k = 2;
                    tr_cnt = 10;
                elseif j>25 && j<=35
                    k = 3;
                    tr_cnt = 25;
                end
            try
                try
					%Clear response validity
                    resp.valid = 0;
					%Reset retry counter
                    retrycounter = 0;
					%Keep querying device until response is correct or max
					%retries is reached.
                    while(resp.valid == 0 && retrycounter <= max_retries)
                        if(retrycounter > 0)
							%If this is not the first attempt, flush buffer and
							%wait 200 ms.
                            flushinput(S)
                            pause(0.2)
                            %disp('.');
                        end
						%Increment retry counter
                        retrycounter = retrycounter + 1;
						%Send ultrasound data query for channel 'c' to device
                        resp = swarm_measure_us(S,addr(cn,k),j-tr_cnt,0);
                    end
                    if(resp.valid == 1 && isfield(resp,'aggregates') && isfield(resp.aggregates,'waveform'))
                        %If response was valid; collect ultrasound data
                        sensor(cn,j).trace = resp.aggregates.waveform;
                        [process_out] = findT_contact(sensor(cn,j),sensor_settings);
                        thick_uncomp=process_out.thick_alt;
                        if thick_uncomp==0
                            thick_comp=nan;
                        else
                            thick_comp=thick_uncomp-thick_uncomp*sensor_settings.temperatureCoefficient*(measurements(1).(sensor)(i).pipe_temp-sensor_settings.temperatureOffset);
                        end
                        sensor(cn,j).thick_comp = thick_comp;
                        sensor(cn,j).thick_comp = thick_uncomp;
                        
                    else
                        sensor(cn,j).thick_comp = [];
                        sensor(cn,j).thick_uncomp = [];
                        sensor(cn,j).trace = 0;
                    end
                    
					%Check if response is structure (to avvoid matlab crashing in case it isn't.)
                    if(isstruct(resp))
                         if(resp.valid ~= 1)
							%If there was an error, notify user. Store result to log file and wait (if errorwaittime is configured).
                            disp(['    Channel ' num2str(c) ': Error.']);
                            %disp(fid,'%s,%s,%s,%u,%s,%u,%u,%s,%s\n',timestring, segment_sn{cn}, serialnumbers{cn, j}, addr(cn, j), 'Ultrasound', c,   resp.valid, cell2mat(resp.errormsg), '');
                            disp(errorwaitmsg);
                            pause(errorwaittime);
                            flushinput(S);                      
                         else
                            %If measurement was successful..
							%Store result to log file
                           % fprintf(fid,'%s,%s,%s,%u,%s,%u,%u,%s,%s,%s\n',timestring, segment_sn{cn}, serialnumbers{cn, j}, addr(cn, j), 'Ultrasound', c,   resp.valid, cell2mat(resp.errormsg), ['[' num2str(data) ']'], num2str(thickness_data));
                            %Decrement retrycounter (because the for-loop had added
							%one extra)
							retrycounter = retrycounter - 1;
							%Print result
							%If retries were needed, inform user.
                            if(retrycounter > 0)
                                disp(['    Channel ' num2str(c) ': Success! (' num2str(retrycounter) ' retries)']);
                            else
                                disp(['    Channel ' num2str(c) ': Success!']);
                            end
                         end
                    else
                        
                    
                    
                    
                
        
                
                
                        
                

