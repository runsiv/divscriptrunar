% Script for live plotting multiple swarm transducers at once
clear
delete(instrfind)
ser_com;
prompt = 'Enter number of swarms: ';

Scnt = input(prompt);



for i = 1:Scnt
    prompt = 'Enter address of swarm: ';

    add(i) = input(prompt);
end
% swarm 1 = 10 trands
% swarm 2 = 15 trans
% swarm 3 = 10 trans 
if Scnt == 1
    prompt = 'Enter start channel of tranducers: ';
    tstart = input(prompt);
    prompt = ' Enter end channel for the transducers';
    tend = input(prompt);
end


if Scnt == 1
    while(1)
        %for i=1:Scnt
            for transcnt=tstart:tend
            resp(transcnt) = swarm_measure_us(ser,add(i),transcnt,1);

            subplot(4,4,(transcnt))

            pt= transcnt;

            plot(resp(transcnt).aggregates.waveform);

            title(['t', num2str(pt)])
            end
        %end

         drawnow();
    end
end

if Scnt == 3
%     while(1)
        
            for transcnt = 1:10
              
                subplot(8,8,(transcnt))
                resp(transcnt) = swarm_measure_us(ser,add(1),transcnt,1);
                pt= transcnt;

                plot(resp(transcnt).aggregates.waveform);
                title(['S1: ', num2str(pt)])
            end
            for transcnt = 1:15
              
                subplot(8,8,(transcnt+10))
                 resp(transcnt) = swarm_measure_us(ser,add(2),transcnt,1);
                pt= transcnt;

                plot(resp(transcnt).aggregates.waveform);
                title(['S2: ', num2str(pt)])
            end
            for transcnt=1:10
                
                  subplot(8,8,(transcnt+25))
                 resp(transcnt) = swarm_measure_us(ser,add(3),transcnt,1);
                pt= transcnt;

                plot(resp(transcnt).aggregates.waveform);
                title(['S3: ', num2str(pt)])
            end
            drawnow();
%     end
end

                
                